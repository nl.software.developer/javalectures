package com.oreillyauto.java.week1.day01;

import com.oreillyauto.java.helpers.DateTimeHelper;
import com.oreillyauto.java.week1.day01.classes.DefaultClockTimex;

public class W5D1TestHarness {

    public W5D1TestHarness() {
    	/* PUBLIC */
        // Test the clock with a public access modifier
    	// For the fun of it, we will convert the Time for this 
    	// example. Going forward we will simply return a "Long."
        ClockReader clockReader = new ClockReader();
        Long time = clockReader.readClock();
        String timeStr = DateTimeHelper.getTime(time, "HH:mm:ss.SSS"); 
        //System.out.println("time: " + time + " timeStr: " + timeStr);
        
        // FYI - Template Literal "Like" Syntax
        // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
        //System.out.println(String.format("time: %d timeStr: %s", time, timeStr));
        //System.out.printf("time: %d timeStr: %s", time, timeStr);  //;)   .printf(String.format("time: %d timeStr: %s", time, timeStr));
        
        /* PROTECTED */
        // Take 1 (Subclass)
    	// Visit SmartProtectedClock.java: change "public long time" 
    	// to "protected long time"
        // Test the clock with a protected access modifier
        // View WorldClass.java for this example
    	// The code will not compile since the member is protected 
    	
    	// Take 2
    	// If we extend another class (subclass), we have access to the
    	// protected super class members
    	// View SmartClock.java
    	// When "this" is printed, does it refer to SmartClock or
    	// SmartProtectedClock?
        //SmartClock mySmartClock = new SmartClock();
        //System.out.println(mySmartClock.time);
        ///System.out.println(mySmartClock.getTimeInSeconds());
        
        /* DEFAULT */
        // Test the Default Clock Reader
        ///DefaultClockTimex timex = new DefaultClockTimex();
        ///System.out.println("timex.time:" + timex.time); 
        // ^== ERROR: The field DefaultClock.time is not visible
        
        /* PRIVATE */
        // Visit Clock.java: change "public long time" to 
    	//    "private long time"
        // We can only access "time" from within the class with 
    	//   this modifier
    	// ClockReader.java will not compile!
    }

    public static void main(String[] args) {
        new W5D1TestHarness();
    }

}
