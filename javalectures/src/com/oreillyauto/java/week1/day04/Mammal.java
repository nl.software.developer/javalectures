package com.oreillyauto.java.week1.day04;

class Mammal extends Animal {
	int debug = 0;
	
	public Mammal() {
		if (debug > 0) System.out.println("Mammal constructor called!");
	}

	public Mammal(String j) {
		super(0, j);
		if (debug > 0) System.out.println("Mammal constructor called");
	}

	public String animalTime() {
		return "It's mammal time!";
	}

	public String invokeSuperTime() {
		return super.animalTime();
	}
}
