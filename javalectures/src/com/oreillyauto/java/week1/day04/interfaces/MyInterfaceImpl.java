package com.oreillyauto.java.week1.day04.interfaces;

public class MyInterfaceImpl implements MyInterface {

    public MyInterfaceImpl() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doStuff() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void dontDoStuff() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void win() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void winHarder() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void doSomething(String a, String b) {
        // TODO Auto-generated method stub
        
    }

//    public String doSomethingElse(String x, String y) {
//        return x + " -> " + y;
//    }

    public String doSomethingElse(String y) {
        return "  -> " + y;
    }
    
}
