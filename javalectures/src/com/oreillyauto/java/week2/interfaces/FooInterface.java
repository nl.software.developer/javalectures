package com.oreillyauto.java.week2.interfaces;

public interface FooInterface {
	public final static String BAR = "Bar";
	
	static String getFoo() {
		return null;
	}
	
	public default String moreFoo() {
		return getFoo();
	}
	
	public abstract String getBar();
	
}
