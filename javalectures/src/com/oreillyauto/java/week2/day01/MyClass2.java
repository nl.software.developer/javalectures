package com.oreillyauto.java.week2.day01;

public class MyClass2 {
	private static int nonStaticVariable = 0;

	public MyClass2() {
        MyInnerClass mic = new MyInnerClass();
        System.out.println(mic.getInnerMethod());
   }

	public static class MyInnerClass {
		
		public MyInnerClass() {
			
		}
		
		protected static final int getInnerMethod() {
			// Remove "static" from class/method to access "nonStaticVariable"  - OR - 
			// Declare the variable "nonStaticVariable to be static
			// Otherwise we get a compilation error:
			// Cannot make a static reference to the non-static field nonStaticVariable
			int increment = nonStaticVariable += 1;
			return increment;
			
			//return 0;
		}
	}

	public static void main(String[] args) {
		new MyClass2();
	}
}
