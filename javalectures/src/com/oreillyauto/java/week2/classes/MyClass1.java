package com.oreillyauto.java.week2.classes;

public class MyClass1 {
    
    private static int nonStaticVariable  = 0;
    
    public MyClass1() {
        MyInnerClass mic = new MyInnerClass();
        System.out.println(MyInnerClass.getInnerMethod());
    }
    
    public static class MyInnerClass {
        protected static final int getInnerMethod() {
            int increment = nonStaticVariable += 1;
            return increment;    
        }
    }
    
    public static void main(String[] args) {
        new MyClass1();
    }
}